package com.mushynskyi;

import com.mushynskyi.view.View;

public class Application {
  public static void main(String[] args) {
    new View().printMenu();
  }
}
