package com.mushynskyi.controller;

import com.mushynskyi.model.CompanyManager;
import com.mushynskyi.model.ICompany;

public class Controller implements IController {

  private final ICompany model;

  public Controller() {
    model = new CompanyManager();
  }

  @Override
  public double getTotalLoadCapacity() {
    return model.getTotalLoadCapacity();
  }

  @Override
  public int getTotalCapacity() {
    return model.getTotalCapacity();
  }

  @Override
  public void printListOfPlane() {
    model.printListOfPlane();
  }

  @Override
  public String sortByRangeOfFlight() {
    return model.sortByRangeOfFlight();
  }

  @Override
  public void printListByRangeOfVolumeOfFuel(double startRangeOfVolume, double endRangeOfVolume) {
    model.printListByRangeOfVolumeOfFuel(startRangeOfVolume, endRangeOfVolume);
  }
}
