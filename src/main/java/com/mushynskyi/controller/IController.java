package com.mushynskyi.controller;

public interface IController {

  double getTotalLoadCapacity();

  int getTotalCapacity();

  void printListOfPlane();

  String sortByRangeOfFlight();

  void printListByRangeOfVolumeOfFuel(double startRangeOfVolume, double endRangeOfVolume);
}
