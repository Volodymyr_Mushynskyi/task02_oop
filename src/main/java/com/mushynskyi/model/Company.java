package com.mushynskyi.model;

import java.util.ArrayList;
import java.util.List;

public class Company {

  private final List<Plane> listOfPlane = new ArrayList<>();

  List<Plane> getListOfPlane() {
    return listOfPlane;
  }

  boolean addPlane(Plane o) {
    return listOfPlane.add(o);
  }
}
