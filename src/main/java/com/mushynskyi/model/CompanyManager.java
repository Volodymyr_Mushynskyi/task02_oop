package com.mushynskyi.model;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CompanyManager implements ICompany {

  private final Company company;

  public CompanyManager() {
    company = new Company();
    createPlane();
  }

  private void createPlane() {
    company.addPlane(new Plane(1, "AirBus748", 100, 576, 6000, 56));
    company.addPlane(new Plane(2, "AirBus788", 1000, 56, 3000, 1200));
    company.addPlane(new Plane(3, "AirBus738", 10, 120, 2000, 35));
  }

  @Override
  public void printListOfPlane() {
    System.out.println(company.getListOfPlane().toString()
            .replace("[", "")
            .replace("]", "")
            .replace(", P", "P"));
  }

  @Override
  public double getTotalLoadCapacity() {
    return company.getListOfPlane().stream()
            .mapToDouble(Plane::getLoadCapacity)
            .sum();
  }

  @Override
  public int getTotalCapacity() {
    return company.getListOfPlane().stream()
            .mapToInt(Plane::getCapacity)
            .sum();
  }

  @Override
  public String sortByRangeOfFlight() {
    return company.getListOfPlane().stream()
            .sorted(Comparator.comparingDouble(Plane::getRangeOfFlight))
            .collect(Collectors.toList()).toString().replace("[", "")
            .replace("]", "")
            .replace(", P", "P");
  }

  private List<Plane> findPlaneByRangeOfVolumeOfFuel(double startRangeOfVolume, double endRangeOfVolume) {
    return company.getListOfPlane().stream()
            .filter(s -> (s.getVolumeOfFuel() > startRangeOfVolume && s.getVolumeOfFuel() < endRangeOfVolume))
            .collect(Collectors.toList());
  }

  @Override
  public void printListByRangeOfVolumeOfFuel(double startRangeOfVolume, double endRangeOfVolume) {
    if (findPlaneByRangeOfVolumeOfFuel(startRangeOfVolume, endRangeOfVolume).isEmpty()) {
      System.out.println("Haven't plane in this range");
    } else {
      System.out.print(findPlaneByRangeOfVolumeOfFuel(startRangeOfVolume, endRangeOfVolume).toString()
              .replace("[", "")
              .replace("]", "")
              .replace(", P", "P"));
    }
  }
}
