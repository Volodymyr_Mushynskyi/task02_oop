package com.mushynskyi.model;

public interface ICompany {

  double getTotalLoadCapacity();

  int getTotalCapacity();

  void printListOfPlane();

  String sortByRangeOfFlight();

  void printListByRangeOfVolumeOfFuel(double startRangeOfVolume, double endRangeOfVolume);
}
