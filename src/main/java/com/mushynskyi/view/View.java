package com.mushynskyi.view;

import com.mushynskyi.controller.Controller;
import com.mushynskyi.controller.IController;

import java.util.Scanner;

public class View {

  private final IController controller;

  private final Scanner scanner = new Scanner(System.in);

  public View() {
    controller = new Controller();
  }

  public void printMenu() {
    double startRangeOfFuel;
    double endRangeOfFuel;
    byte choose;

    while (true) {
      System.out.println("----------------------MENU---------------------------------------------------");
      System.out.println("Press 1 Counting general capacity and load capacity");
      System.out.println("Press 2 Sorting plane by range of flight");
      System.out.println("Press 3 Print all planes in the company");
      System.out.println("Press 4 Find plane by range of fuel");
      System.out.println("Press 0 Exit");
      choose = scanner.nextByte();
      switch (choose) {
        case 1:
          System.out.println("Total capacity :" + controller.getTotalCapacity());
          System.out.println("Total load capacity :" + controller.getTotalLoadCapacity());
          break;
        case 2:
          System.out.println("Sorted plane :" + "\n" + controller.sortByRangeOfFlight());
          break;
        case 3:
          controller.printListOfPlane();
          break;
        case 4:
          System.out.print("Write your start range of fuel :");
          startRangeOfFuel = scanner.nextDouble();
          System.out.print("Write your end range of fuel :");
          endRangeOfFuel = scanner.nextDouble();
          controller.printListByRangeOfVolumeOfFuel(startRangeOfFuel, endRangeOfFuel);
          break;
        case 0:
          return;
        default:
      }
    }
  }
}
